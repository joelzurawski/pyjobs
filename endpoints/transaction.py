from flask_restplus import Resource
from serializers.transaction import transaction as serializer_transaction
from business.application.restplus import api
from business.transaction.control_transaction import ControlTransaction
from business.transaction.query_transaction import QueryTransaction


transaction = api.namespace('api/v1/transaction')


@transaction.route('/', methods=['POST'])
class RouteInsertTransaction(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @transaction.response(400, 'Request Error')
    @transaction.response(500, 'Server Error')
    @transaction.response(422, 'Payload Error')
    @transaction.expect(serializer_transaction, validate=True)
    # valiar o retorno do métdo post
    # @teste.marshal_with(compute_result, code=200)
    def post(self):
        """
            Popular payment data.
            Receive payload with payment details.
        """
        control = ControlTransaction()
        result = control.insert(json_transaction=api.payload)
        return result


@transaction.route('/establishment/<string:cnpj>', methods=['GET'])
class RouteGetTransactions(Resource):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @transaction.response(400, 'Request Error')
    @transaction.response(422, 'Payload Error')
    @transaction.response(500, 'Server Error')
    def get(self, cnpj):
        """
            Return transactions by filtering by the establishment's cnpj
        :param cnpj: Receive numbers only, example 45283163000167
        :return: Object json
        """
        query = QueryTransaction()
        results = query.get_establishment_transactions(cnpj=cnpj)
        return results

from flask import Blueprint
from business.application.restplus import api
from endpoints.transaction import transaction


blueprint = Blueprint('api', __name__)
api.init_app(blueprint)

api.add_namespace(transaction)

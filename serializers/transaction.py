from business.application.restplus import api
from flask_restplus import fields


transaction = api.model('Transaction', {
    'establishment': fields.String(required=True, description='CNPJ regarding the establishment'),
    'client': fields.String(required=True, description='CPF regarding th establishment'),
    'value': fields.Float(required=True, description='Transaction value'),
    'description': fields.String(required=True, description='Brief description regarding payment')
})

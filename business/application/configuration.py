"""
    This file aims to centralize the settings of the api
"""

RESTPLUS_VALIDATE = True
ERROR_404_HELP = False
RESTPLUS_MASK_SWAGGER = False

CORS_HEADERS = 'Content-Type'

SYS_TITLE = 'Python Challange'
SYS_VERSION = '1.0.0'
SYS_DESCRIPTION = 'PyJobs Python Challenge - Joel Luís Zurawski'

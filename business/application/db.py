import os
from flask_sqlalchemy import SQLAlchemy

db_SQLAlchemy = SQLAlchemy()


class ConfigureDatabaseConnection(object):
    def __init__(self, app):
        self.__app = app

    def to_set_up_db_flask_sqlalchemy(self):
        try:
            file_name = 'pyjobs.db'
            path = os.path.abspath(os.getcwd())+'/database/'
            uri = f'sqlite:///{path}{file_name}'
            self.__app.config['SQLALCHEMY_DATABASE_URI'] = uri
            self.__app.config['SQLALCHEMY_BINDS'] = {
                'sqlLite': uri
            }
            self.__app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
            self.__app.config['SQLALCHEMY_POOL_RECYCLE'] = 500


            db_SQLAlchemy.init_app(self.__app)
            db_SQLAlchemy.app = self.__app
            db_SQLAlchemy.create_all()
        except Exception as erro:
            print(f'OS error: {format(erro)}')
            raise

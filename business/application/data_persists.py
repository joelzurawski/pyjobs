from business.application.db import db_SQLAlchemy as Db


class DataPersists(object):
    def record(self, objeto):
        try:
            Db.session.session_factory()
            Db.session.add(objeto)
            Db.session.commit()
            return objeto
        except Exception:
            Db.session.remove()
            raise

    def delete(self, objeto):
        try:
            Db.session.session_factory()
            Db.session.delete(objeto)
            Db.session.commit()
            return True
        except Exception:
            Db.session.remove()
            raise

"""
    Has the function of increasing one month,
    using the flask_restplus framework
"""

from flask_restplus import Api
from business.application import configuration


api = Api(title=configuration.SYS_TITLE,
          version=configuration.SYS_VERSION,
          description=configuration.SYS_DESCRIPTION)

class InvalidPayload(Exception):
    @staticmethod
    def exception_customization_pyload(response):
        """
            Used to customize exception when submitting incorrect payload
            param: response: The response with the exception
        """

        if response.json is not None and 'payload' in response.json.get('message'):
            response.status_code = 422
        return response


class InvalidUsage(Exception):
    """
        The invalid use has the functionality to customize friendly messages for
        the understanding of the user.
        
        param: message: Used for message returned to the user
        param: status_code: Used to change response status code, if empty the default is 400
        
    """
    
    __status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.__status_code = status_code
        self.payload = payload

    def to_dict(self):
        """
            Returns custom message
        """
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

    def get_status_code(self):
        """
            Returns status code
        """
        return self.__status_code

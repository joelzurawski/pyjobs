from models.establishment import Establishment
from bradocs4py import Cnpj
from ..transaction.json_transactions import JsonTransactions


class QueryTransaction(object):
    def get_establishment_transactions(self, cnpj):
        """
            Responsible for searching information of the establishment
             and transactions in the database, filtering by cnpj
        :param cnpj: Receives cnpj string format only numbers
        :return: Object json
        """
        establishment = Establishment()
        cnpj = str(Cnpj(cnpj))
        establishment = establishment.query.filter_by(cnpj=cnpj).first()
        json = JsonTransactions().get_json(establishment=establishment)
        return json

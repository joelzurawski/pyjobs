from models.transaction import Transaction
from models.establishment import Establishment
from ..application.data_persists import DataPersists
from bradocs4py import CPF, Cnpj


class ControlTransaction(object):
    """
        Object responsible for inserting transactions in the database
    """

    def insert(self, json_transaction):
        """
        :param json_transaction: receives json object containing the data
        :return: json object informing if it was inserted with a value of True otherwise False
        """
        cpf = json_transaction.get('client')
        cnpj = json_transaction.get('establishment')

        if CPF(cpf).isValid is False or Cnpj(cnpj).isValid is False:
            return {"accept": False}

        transaction = Transaction()
        transaction.client = json_transaction.get('client')
        transaction.description = json_transaction.get('description')
        transaction.value = json_transaction.get('value')
        transaction.establishment = self.__get_establishment(cnpj=cnpj)
        persist = DataPersists()
        persist.record(transaction)
        return {"accept": True}

    def __get_establishment(self, cnpj):
        """
            Search object of establishment, filtering by cnpj
        :param cnpj: Receive string with cnpj
        :return: Object establishment
        """
        establishment = Establishment()
        establishment = establishment.query.filter_by(cnpj=cnpj).first()
        return establishment

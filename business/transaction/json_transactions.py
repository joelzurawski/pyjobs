from models.establishment import Establishment


class JsonTransactions(object):
    def __init__(self):
        self.__json = {}

    def get_json(self, establishment: Establishment):
        """
            Method responsible for returning the establishment's
            json with its transactions and the total amount
        :param establishment: receive object Establishment
        :return: Object json
        """
        json_establishment = {'name': establishment.name,
                              'cnpj': establishment.cnpj,
                              'owner': establishment.owner,
                              'telephone': establishment.telephone}

        self.__json['establishment'] = json_establishment
        receipts, amount = self.__get_transactions(establishment.transactions)
        self.__json['receipts'] = receipts
        self.__json['total_receipt'] = amount
        return self.__json

    def __get_transactions(self, transactions):
        """
            Process transaction list and return in json format
        :param transactions: List containing transactions
        :return: object json and amount
        """
        list_transactions = []
        amount = 0
        for transaction in transactions:
            json_transaction = {"client": transaction.client,
                                "value": float(round(transaction.value, 2)),
                                "description": transaction.description}
            list_transactions.append(json_transaction)
            amount += transaction.value

        return list_transactions, float(round(amount, 2))

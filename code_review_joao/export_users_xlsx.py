import xlsxwriter
import os
from datetime import datetime


class ExportUsersXlsx(object):
    def __init__(self, db):
        self.__db = db

    def export(self):
        workbook = self.__get_worksheet()
        worksheet = workbook.add_worksheet()
        self.__set_coluns_worksheet(worksheet=worksheet)

        users = self.__get_users()

        index = 1
        for user in users:
            index += 1
            print('Id: {0}'.format(user[0]))
            worksheet.write('A{0}'.format(index), user[0])
            print('Name: {0}'.format(user[1]))
            worksheet.write('B{0}'.format(index), user[1])
            print('Email: {0}'.format(user[2]))
            worksheet.write('C{0}'.format(index), user[2])
            print('Password: {0}'.format(user[3]))
            worksheet.write('D{0}'.format(index), user[3])
            print('Role Id: {0}'.format(user[4]))
            worksheet.write('E{0}'.format(index), user[4])
            print('Created At: {0}'.format(user[5]))
            worksheet.write('F{0}'.format(index), user[5])
            print('Updated At: {0}'.format(user[6]))
            worksheet.write('G{0}'.format(index), user[6])
        workbook.close()

    def __get_worksheet(self):
        file_name = 'data_export_{0}.xlsx'.format(datetime.now().strftime("%Y%m%d%H%M%S"))
        file_path = os.path.join(os.path.curdir, file_name)
        workbook = xlsxwriter.Workbook(file_path)
        return workbook

    def __set_coluns_worksheet(self, worksheet):
        index = 1
        worksheet.write('A{0}'.format(index), 'Id')
        worksheet.write('B{0}'.format(index), 'Name')
        worksheet.write('C{0}'.format(index), 'Email')
        worksheet.write('D{0}'.format(index), 'Password')
        worksheet.write('E{0}'.format(index), 'Role Id')
        worksheet.write('F{0}'.format(index), 'Created At')
        worksheet.write('G{0}'.format(index), 'Updated At')

    def __get_users(self):
        sql = 'SELECT * FROM users;'
        users = self.__db.session.execute(sql)
        return users

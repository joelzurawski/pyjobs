from export_users_xlsx import ExportUsersXlsx
from apscheduler.schedulers.blocking import BlockingScheduler


class JobExportUsers(object):
    def __task1(self, db):
        users_xlsx = ExportUsersXlsx(db)
        users_xlsx.export()
        print('job executed!')

    def create_job(self, db, minutes):
        scheduler = BlockingScheduler()
        scheduler.add_job(self.__task1(db), 'interval', id='task1_job', minutes=minutes)
        try:
            scheduler.start()
        except(KeyboardInterrupt, SystemExit):
            pass

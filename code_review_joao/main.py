# -*- coding: utf-8 -*-
import os
import logging
import configparser
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from logging.handlers import RotatingFileHandler
from job_export_users import JobExportUsers


class ExportData(object):
    def __greetings(self):
        print('             ##########################')
        print('             # - ACME - Tasks Robot - #')
        print('             # - v 1.0 - 2020-07-28 - #')
        print('             ##########################')

    def __configure_log(self, app):
        handler = RotatingFileHandler('bot.log', maxBytes=10000, backupCount=1)
        handler.setLevel(logging.INFO)
        app.logger.addHandler(handler)

    def __get_db(self, app):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:123mudar@127.0.0.1:5432/bot_db'
        return SQLAlchemy(app)

    def __get_config(self):
        config = configparser.ConfigParser()
        config.read('/tmp/bot/settings/config.ini')
        return config

    def __set_warning_interval_minutes_log(self, minutes, app):
        app.logger.warning('Intervalo entre as execucoes do processo: {}'.format(minutes))

    def execute(self):
        self.__greetings()

        print('Press Crtl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

        app = Flask(__name__)

        self.__configure_log(app)

        config = self.__get_config()
        interval_minutes = int(config.get('scheduler', 'IntervalInMinutes'))
        self.__set_warning_interval_minutes_log(minutes=interval_minutes, app=app)

        db = self.__get_db(app)
        job = JobExportUsers()
        job.create_job(minutes=interval_minutes, db=db)


if __name__ == '__main__':
    ExportData().execute()

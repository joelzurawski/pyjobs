"""
    This application is the solution to the challenge of the Python opportunity at PyJobs.
    Candidate: Joel Luís Zurawski

    app.py is used to upload the api service
"""

from flask import Flask, jsonify
from business.application import configuration
from business.application.custom_exception import InvalidPayload, InvalidUsage
from endpoints.register_blueprint import blueprint
from business.application.db import ConfigureDatabaseConnection


app = Flask(__name__)
app.config.from_object(configuration)

app.register_blueprint(blueprint)

ConfigureDatabaseConnection(app).to_set_up_db_flask_sqlalchemy()

@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.get_status_code()
    return response


@app.after_request
def after_request(response):
    response.headers.add('X-Frame-Options', 'deny')
    response.headers.add('X-Content-Type-Options', 'nosniff')
    # cache
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = 0

    if int(response.status_code) >= 400:
        exception = InvalidPayload()
        exception.exception_customization_pyload(response=response)

    return response

from business.application.db import db_SQLAlchemy as Db
from models.establishment import Establishment


class Transaction(Db.Model):
    __bind_key__ = 'sqlLite'
    __tablename__ = 'transactions'

    id = Db.Column('id', Db.Integer, primary_key=True)
    id_establishment = Db.Column('id_establishment', Db.Integer,
                                 Db.ForeignKey('establishment.id'))
    client = Db.Column('client', Db.String(14))
    value = Db.Column('value', Db.Numeric(scale=10, precision=2), nullable=False)
    description = Db.Column('description', Db.String(500), nullable=False)
    establishment = Db.relationship(Establishment, backref=__tablename__, lazy=True)

from business.application.db import db_SQLAlchemy as Db


class Establishment(Db.Model):
    __bind_key__ = 'sqlLite'
    __tablename__ = 'establishment'

    id = Db.Column('id', Db.Integer, primary_key=True)
    cnpj = Db.Column('cnpj', Db.String(18))
    name = Db.Column('name', Db.String(255))
    owner = Db.Column('owner', Db.String(100))
    telephone = Db.Column('telephone', Db.String(14))

from unittest import TestCase
from flask_restplus import Namespace
from business.application.custom_exception import InvalidPayload, InvalidUsage


class TestInvalidPayload(TestCase):
    def test_exception_customization_pyload(self):
        invalid = InvalidPayload()
        status_code_test = 422
        message = 'test exception customization payload'
        response = Namespace('teste').response(code=400, description=message)
        response.status_code = 400
        response.json = {'message': message}
        response_invalid = invalid.exception_customization_pyload(response)
        self.assertEqual(response_invalid.status_code, status_code_test)


class TestInvalidUsage(TestCase):
    def test_to_dict(self):
        message = 'test custom exception invalid usage'
        invalid = InvalidUsage(message=message)
        self.assertEqual(invalid.to_dict(), {'message': message})

    def test_get_status_code(self):
        invalid = InvalidUsage(message='test custom exception invalid usage', status_code=404)
        self.assertGreaterEqual(invalid.get_status_code(), 400)
        invalid = InvalidUsage(message='test custom exception invalid usage')
        self.assertEqual(invalid.get_status_code(), 400)

from unittest import TestCase
from business.application.data_persists import DataPersists
from app import app
from models.establishment import Establishment


class TestDataPersists(TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def tearDown(self) -> None:
        pass

    def test_record(self):
        establishment = Establishment()
        establishment.telephone = '1135221010'
        establishment.name = 'Unit Test'
        establishment.owner = 'Test'
        establishment.cnpj = '25.980.232/0001-84'
        persist = DataPersists()
        establishment = persist.record(objeto=establishment)
        self.assertEqual(type(establishment), Establishment)
        self.assertGreaterEqual(establishment.id, 0)

    def test_record_exception(self):
        establishment = {}
        persist = DataPersists()
        with self.assertRaises(Exception):
            persist.record(objeto=establishment)

    def test_delete(self):
        establishment = Establishment()
        establishments = establishment.query.filter_by(cnpj='25.980.232/0001-84').all()
        persist = DataPersists()
        result = False
        for establishment in establishments:
            result = persist.delete(objeto=establishment)
        self.assertTrue(result)

    def test_delete_exception(self):
        establishment = {}
        persist = DataPersists()
        with self.assertRaises(Exception):
            persist.delete(objeto=establishment)

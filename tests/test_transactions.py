from unittest import TestCase
from app import app
from flask import json


class TestRouteComputeExperience(TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def tearDown(self) -> None:
        pass

    def test_post(self):
        json_input = {
            "establishment": "45.283.163/0001-67",
            "client": "094.214.930-01",
            "value": 100.10,
            "description": "Testing"
        }
        response = self.app.get('/', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        response = self.app.post('/api/v1/transaction/',
                                 data=json.dumps(json_input),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 200)
        json_input = {}
        response = self.app.post('/api/v1/transaction/',
                                 data=json.dumps(json_input),
                                 content_type='application/json')
        self.assertEqual(response.status_code, 422)

    def test_get_establishment(self):
        response = self.app.get('/api/v1/transaction/establishment/45283163000167')
        self.assertEqual(response.status_code, 200)

from unittest import TestCase
from app import app
from business.application.db import ConfigureDatabaseConnection


class TestConfigureDatabaseConnection(TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def tearDown(self) -> None:
        pass

    def test_to_set_up_db_flask_sqlalchemy(self):
        self.app = None
        connection = ConfigureDatabaseConnection(app=self.app)
        with self.assertRaises(Exception):
            connection.to_set_up_db_flask_sqlalchemy()

from unittest import TestCase
from business.transaction.control_transaction import ControlTransaction
from app import app


class TestControlTransaction(TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def tearDown(self) -> None:
        pass

    def test_insert(self):
        json_input = {
            "establishment": "45.283.163/0001-67",
            "client": "094.214.930-01",
            "value": 100.10,
            "description": "Testing"
        }
        transacrtion = ControlTransaction()
        result = transacrtion.insert(json_input)
        self.assertEqual(result, {"accept": True})

        transacrtion = ControlTransaction()
        json_input["client"] = '094.214.930-55'
        result = transacrtion.insert(json_input)
        self.assertEqual(result, {"accept": False})
